import React, { Component } from "react";
import Login from "./components/Mainpage/login";
import Meeting from "./components/Mainpage/Meeting";
import Home from "./components/Mainpage/Home";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import { isAuthenticated } from "./components/Mainpage/repository";
import Footer from "./components/Mainpage/footer";
import IssueForm from "./components/IssueComponents/issue-form";
import AnalysisPage from "./components/Mainpage/analysisPage";
import IssueAnalysis from "./components/IssueComponents/issue-view";

class App extends Component {
	logOut() {
		localStorage.removeItem("x-access-token");
	}

	render() {
		return (
			<Router>
				<div>
					<nav className="navigation">
						<div className="navbar navbar-expand navbar-dark bg-dark">
							<div className="container">
								<Link class="navbar-brand" to="/">
									{" "}
									Return to Home Page
								</Link>
							</div>

							<ul className="navbar-nav ml-auto">
								<li>
									<Link class="nav-link" to="/issues">
										Issues
									</Link>
								</li>

								<li>
									<Link class="nav-link" to="/meetings">
										Meetings
									</Link>
								</li>

								<li>
									{isAuthenticated() ? (
										<Link class="nav-link" to="/analysis">
											Analysis
										</Link>
									) : (
										""
									)}
								</li>
							</ul>
							<ul className="navbar-nav ml-auto">
								{isAuthenticated() ? (
									<li onClick={this.logOut}>
										<a href="/">Logout</a>{" "}
									</li>
								) : (
									<li>
										<Link class="nav-link" to="/login">
											Login
										</Link>
									</li>
								)}
							</ul>
						</div>
					</nav>
					<Route exact path="/" component={Home} />
					<Route exact path="/issues" component={IssueForm} />
					<Route exact path="/meetings">
						<Meeting></Meeting>
					</Route>
					<Route exact path="/analysis">
						<AnalysisPage></AnalysisPage>
						<IssueAnalysis></IssueAnalysis>
					</Route>
					<Route exact path="/login" component={Login} />
				</div>
				<br></br>
				<br></br>
				<br></br>
				<br></br>
				<br></br>
				<br></br>
				<br></br>
				<Footer></Footer>
			</Router>
		);
	}
}

export default App;
