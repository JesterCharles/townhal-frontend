import axios from "axios";
import { useEffect, useRef } from "react";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import { Container } from "react-bootstrap";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";

export default function IssueForm() {
	const urlII = "https://issue-pub-run-jjer53nvda-uc.a.run.app/submitissue";
	const dateOfIssueInput = useRef(null);
	const issueDescriptionInput = useRef(null);
	const locationInput = useRef(null);
	const issueTypeInput = useRef(null);

	useEffect(() => {
		axios.get("https://issue-sub-run-jjer53nvda-uc.a.run.app/");
	}, []);

	async function addIssue() {
		const issue = {
			datePosted: new Date().toISOString(),
			dateOfIssue: dateOfIssueInput.current.value,
			issueDescription: issueDescriptionInput.current.value,
			location: locationInput.current.value,
			issueType: issueTypeInput.current.value,
			reviewed: false,
			highlighted: false
		};

		const response = await axios.post(`${urlII}`, issue);
		alert(response.data);
		window.location.reload(false);
	}

	return (
		<Container>
			<Box sx={{ width: "75%", p: 1 }}>
				<Typography variant="h4">Issue Form</Typography>
				<Divider />
				<Typography variant="h6">Use the form below to submit an issue you would like reviewed by a Council Member:</Typography>
				<Divider />
				<div></div>
				<br></br>
				<InputLabel id="date-of-issue">Date of Issue</InputLabel>
				<TextField
					labelId="date-of-issue"
					sx={{ p: 1 }}
					placeholder="Date of Issue"
					inputRef={dateOfIssueInput}
					type="date"
				></TextField>
				<div></div>
				<TextField fullWidth sx={{ p: 1 }} placeholder="Issue Description" inputRef={issueDescriptionInput}></TextField>
				<div></div>
				<TextField sx={{ p: 1 }} placeholder="Location" inputRef={locationInput}></TextField>
				<div></div>
				<Box>
					<FormControl fullWidth sx={{ p: 1 }}>
						<InputLabel id="demo-simple-select-label">Issue Type</InputLabel>
						<Select labelId="demo-simple-select-label" id="demo-simple-select" inputRef={issueTypeInput} label="Issue Type">
							<MenuItem value={"Infrastructure"}>Infrastructure</MenuItem>
							<MenuItem value={"Safety"}>Safety</MenuItem>
							<MenuItem value={"Public Health"}>Public Health</MenuItem>
							<MenuItem value={"Pollution"}>Pollution</MenuItem>
							<MenuItem value={"Disturbance"}>Disturbance</MenuItem>
							<MenuItem value={"Other"}>Other</MenuItem>
						</Select>
					</FormControl>
				</Box>
				<Button onClick={addIssue}>Add Issue</Button>
			</Box>
		</Container>
	);
}
