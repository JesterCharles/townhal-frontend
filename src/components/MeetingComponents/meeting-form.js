import axios from "axios";
import { useRef, useState } from "react";

export default function MeetingForm(props) {
	const [meetings, setMeetings] = props.functions;
	const showMeeting = props.show;
	const urlII = "http://35.232.3.28/ms/meetings";
	const types = ["Safety", "Infrastructure", "Public Health", "Pollution", "Disturbance", "Other"];

	const [checkedState, setCheckedState] = useState(new Array(types.length).fill(false));

	const locationInput = useRef();
	const otherValueInput = useRef(null);
	const councilLeadInput = useRef();

	const handleOnChange = (position) => {
		const updatedCheckedState = checkedState.map((item, index) => (index === position ? !item : item));

		setCheckedState(updatedCheckedState);
	};

	async function addMeeting() {
		let tempOtherValue;
		try {
			tempOtherValue = otherValueInput.current.value;
		} catch (error) {
			tempOtherValue = "";
		}
		const meeting = {
			meetingId: 0,
			location: locationInput.current.value,
			safety: checkedState[0],
			Infrastructure: checkedState[1],
			publicHealth: checkedState[2],
			pollution: checkedState[3],
			disturbance: checkedState[4],
			other: checkedState[5],
			otherValue: tempOtherValue,
			councilLead: councilLeadInput.current.value
		};
		const response = await axios
			.post(`${urlII}`, meeting, {
				headers: { "x-access-token": localStorage.getItem("x-access-token") }
			})
			.catch((err) => Promise.reject("Request Not Authenticated!"));

		if (showMeeting) {
			const response2 = await axios.get(`${urlII}`);
			setMeetings(response2.data);
		}
		console.log(response.data);
		setCheckedState(new Array(types.length).fill(false));
		locationInput.current.value = "";
		councilLeadInput.current.value = "";
	}

	return (
		<div>
			<h3>Add Meeting Form</h3>
			<input placeholder="Location" ref={locationInput} />
			<h5>Type(s)</h5>
			{types.map((type, index) => {
				return (
					<li style={{ "list-style": "none" }} key={index}>
						<div>
							<div>
								<input
									type="checkbox"
									id={`custom-checkbox-${index}`}
									name={type}
									value={type}
									checked={checkedState[index]}
									onChange={() => handleOnChange(index)}
								/>
								<label htmlFor={`custom-checkbox-${index}`}>{type}</label>
							</div>
						</div>
					</li>
				);
			})}
			{checkedState[5] ? <input placeholder="Other type" ref={otherValueInput} /> : null}
			<input placeholder="Council Lead" ref={councilLeadInput} />
			<button onClick={() => addMeeting()}>Add Meeting</button>
		</div>
	);
}
