import axios from "axios";
import { useRef, useState } from "react";

export default function PopoverTable(props) {
	const [meetingIntake, setMeetings] = props.functions;
	const specific = props.specific;
	const urlII = "http://35.232.3.28/ms/meetings";
	const types = ["Safety", "Infrastructure", "Public Health", "Pollution", "Disturbance", "Other"];

	const [checkedState, setCheckedState] = useState([
		meetingIntake.safety,
		meetingIntake.Infrastructure,
		meetingIntake.publicHealth,
		meetingIntake.pollution,
		meetingIntake.disturbance,
		meetingIntake.other
	]);

	// const meetingIdInput = useRef();
	const locationInput = useRef();
	const otherValueInput = useRef(null);
	const councilLeadInput = useRef();

	const handleOnChange = (position) => {
		const updatedCheckedState = checkedState.map((item, index) => (index === position ? !item : item));

		setCheckedState(updatedCheckedState);
	};

	async function updateMeeting() {
		let tempOtherValue;
		try {
			tempOtherValue = otherValueInput.current.value;
		} catch (error) {
			tempOtherValue = "";
		}
		const meeting = {
			meetingId: meetingIntake.meetingId,
			location: locationInput.current.value,
			safety: checkedState[0],
			Infrastructure: checkedState[1],
			publicHealth: checkedState[2],
			pollution: checkedState[3],
			disturbance: checkedState[4],
			other: checkedState[5],
			otherValue: tempOtherValue,
			councilLead: councilLeadInput.current.value
		};
		const response = await axios
			.put(`${urlII}/${meetingIntake.meetingId}`, meeting, {
				headers: { "x-access-token": localStorage.getItem("x-access-token") }
			})
			.catch((err) => Promise.reject("Request Not Authenticated!"));

		//const response = await axios.put(`${urlII}/${meetingIntake.meetingId}`, meeting);
		console.log(response.data);
		if (specific) {
			const response2 = await axios.get(`${urlII}/${meeting.meetingId}`);
			const resArray = [response2.data];
			setMeetings(resArray);
		} else {
			const response2 = await axios.get(`${urlII}`);
			setMeetings(response2.data);
		}
		alert("Updated meeting");
	}

	return (
		<div>
			<input placeholder="Location" defaultValue={meetingIntake.location} ref={locationInput} />
			<h5>Type(s)</h5>
			{types.map((type, index) => {
				return (
					<li style={{ "list-style": "none" }} key={index}>
						<div>
							<div>
								<input
									type="checkbox"
									id={`popover-checkbox-${index}`}
									name={type}
									value={type}
									checked={checkedState[index]}
									// onLoad={() = setCheckedState(meetingIntake.type)}
									onChange={() => handleOnChange(index)}
								/>
								<label htmlFor={`popover-checkbox-${index}`}>{type}</label>
							</div>
						</div>
					</li>
				);
			})}
			{checkedState[5] ? <input placeholder="Other type" defaultValue={meetingIntake.otherValue} ref={otherValueInput} /> : null}
			<input placeholder="Council Lead" defaultValue={meetingIntake.councilLead} ref={councilLeadInput} />
			<button onClick={() => updateMeeting()}>Update</button>
		</div>
	);
}
