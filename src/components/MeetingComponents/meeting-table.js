import Popover from "react-bootstrap/Popover";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Button from "react-bootstrap/Button";
import axios from "axios";
import Table from "react-bootstrap/Table";
import PopoverTable from "./popover-table";

export default function MeetingTable(props) {
	const [meetings, setMeetings] = props.functions;
	const specific = props.specific;
	const url = "http://35.232.3.28/ms/meetings";

	async function deleteMeeting(id) {
		await axios
			.delete(`${url}/${id}`, {
				headers: { "x-access-token": localStorage.getItem("x-access-token") }
			})
			.catch((err) => Promise.reject("Request Not Authenticated!"));

		alert(`Meeting ${id} deleted`);
		if (specific) {
			const response2 = await axios.get(`${url}/${id}`);
			setMeetings(response2.data);
		} else {
			const response2 = await axios.get(`${url}`);
			setMeetings(response2.data);
		}
	}

	const Edit = (props) => {
		const meeting = props.meeting;
		return (
			<OverlayTrigger rootClose={true} trigger="click" placement="top" overlay={popover(meeting)}>
				<Button variant="secondary">Edit</Button>
			</OverlayTrigger>
		);
	};

	const popover = (meet) => {
		const meeting = meet;

		return (
			<Popover id="popover-basic">
				<Popover.Header as="h3">Enter New Information</Popover.Header>
				<Popover.Body>
					<PopoverTable functions={[meeting, setMeetings]} specific={specific} />
				</Popover.Body>
			</Popover>
		);
	};

	function TypeString(props) {
		const meeting = props.meeting;
		let types = `${meeting.safety ? "Safety, " : ""}${meeting.Infrastructure ? "Infrastructure, " : ""}${
			meeting.publicHealth ? "Public Health, " : ""
		}
        ${meeting.pollution ? "Pollution, " : ""}
        ${meeting.disturbance ? "Disturbance, " : ""}
        ${meeting.other ? meeting.otherValue : ""}`;
		console.log(types);
		return <td>{types.replace(/,(\s+)?$/, "")}</td>;
	}

	const tRows = meetings
		.sort((a, b) => (a.meetingId > b.meetingId ? 1 : -1))
		.map((m) => (
			<tr key={m.meetingId}>
				<td>{m.meetingId}</td>
				<td>{m.location}</td>
				<TypeString meeting={m} />
				<td>{m.councilLead}</td>
				{localStorage.getItem("x-access-token") ? <button onClick={() => deleteMeeting(m.meetingId)}>Delete</button> : null}
				{localStorage.getItem("x-access-token") ? <Edit meeting={m} /> : null}
			</tr>
		));

	return (
		<Table striped bordered hover>
			<thead>
				<tr>
					<th>Id</th>
					<th>Location</th>
					<th>Type(s) of Issues Covered</th>
					<th>Council Lead</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>{tRows}</tbody>
		</Table>
	);
}
