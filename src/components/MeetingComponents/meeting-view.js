import axios from "axios";
import { useRef, useState } from "react";
import MeetingForm from "./meeting-form";
import MeetingTable from "./meeting-table";

export default function MeetingView() {
	const urlII = "http://35.232.3.28/ms/meetings";
	const meetingId = useRef();
	const [meetings, setMeetings] = useState([]);
	const [showMeetings, setShowMeetings] = useState(false);
	const [showSpecificMeeting, setShowSpecificMeeting] = useState(false);

	async function getMeeting() {
		if (meetingId.current.value) {
			if (showSpecificMeeting === false) {
				const response = await axios.get(`${urlII}/${meetingId.current.value}`);
				console.log(response);
				const resArray = [response.data];
				setMeetings(resArray);
				setShowSpecificMeeting(true);
				setShowMeetings(false);
			} else {
				setShowSpecificMeeting(false);
			}
		} else {
			alert("You must enter a message id");
		}
	}

	async function getAllMeetings() {
		if (showMeetings === false) {
			const response = await axios.get(`${urlII}`);
			console.log(response);
			setMeetings(response.data);
			setShowMeetings(true);
			setShowSpecificMeeting(false);
		} else {
			setShowMeetings(false);
		}
	}

	return (
		<div>
			{localStorage.getItem("x-access-token") ? <MeetingForm functions={[meetings, setMeetings]} show={showMeetings} /> : null}
			<h3>View Meetings</h3>
			<input placeholder="Meeting Id" ref={meetingId}></input>
			<button onClick={getMeeting}>Get Meeting</button>
			<button onClick={getAllMeetings}>Get All Meetings</button>
			{showMeetings || showSpecificMeeting ? (
				<MeetingTable functions={[meetings, setMeetings]} specific={showSpecificMeeting} />
			) : null}
		</div>
	);
}
